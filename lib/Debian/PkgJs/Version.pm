package Debian::PkgJs::Version;

use Exporter 'import';

our $VERSION = '0.14.18';
our @EXPORT = qw($VERSION);

