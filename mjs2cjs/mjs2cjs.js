const pkg = require(process.env.PWD+'/package.json');
if(!pkg.dependencies) pkg.dependencies={};
const file = process.env.DEST_FILE ? process.env.DEST_FILE : pkg.exports && pkg.exports.require ? pkg.exports.require : 'index.cjs';

const config = {
	input: process.env.START_FILE,
	output: [{
		format: 'cjs',
		file: file,
		preferConst: true,
		interop: false,
		freeze: false,
		strict: false
	}],
	external: [
		...Object.keys(pkg.dependencies),
		...require('module').builtinModules,
	],
}

export default config;
